# -*- coding: utf8 -*-

import os
import sys
import csv
import time
from contextlib import contextmanager
from .workbook import Workbook
from . import RESTRICTION_ROW_ON_SHEET, ExeededLimitCol, PY3


@contextmanager
def opened_w_error(filename, mode, encoding):
    try:
        if encoding and PY3:
            f = open(filename, mode, encoding=encoding)
        else:
            f = open(filename, mode)
    except IOError as err:
        yield None, err
    else:
        try:
            yield f, None
        finally:
            f.close()


class CSV2XLSX(object):

    """Conveter csv to xlsx. """

    def __init__(self, csvfile, xlsxfile=None, delimiter=None,
                 quotechar=None, silence=False, codepage=None):

        self.csvfile = csvfile
        self.codepage = codepage
        self.delimiter = delimiter if delimiter is not None else ';'
        self.quotechar = quotechar if quotechar is not None else "'"
        self.silence = silence
        if not xlsxfile:
            self.__select_xlsx_name()

    def convert(self):
        if self.silence:
            self.__do_it_silence()
        else:
            self.__do_it_verbosity()

    def __select_xlsx_name(self):
        """We select the name if it is not specified."""

        self.xlsxfile = '%s%s' % (self.csvfile.split('.csv')[0], '.xlsx')
        i = 1
        while True:
            if os.path.exists(os.path.join(os.getcwd(), self.xlsxfile)):
                self.xlsxfile = '%s(%s)%s' % (self.xlsxfile.split('.xlsx')[0].split('(')[0], i, '.xlsx')
                i += 1
            else:
                break

    def __make_xlsx(self):
        """Is reading csv-file and writing to xlsx-file each row,
        If overflow happens, it adds a new sheet."""

        sheet, correction = 1, 0
        workbook = Workbook(self.xlsxfile, self.codepage)
        worksheet = workbook.add_worksheet()
        begin_converted = time.time()
        with opened_w_error(self.csvfile, 'rt', encoding=self.codepage) as (f, err):
            if err:
                sys.stderr.write(str(err) + '\n')
                sys.exit(1)
            reader = csv.reader(f, delimiter=self.delimiter, quotechar=self.quotechar)
            for row_number, row in enumerate(reader):
                try:
                    worksheet.append_row(row)
                except ExeededLimitCol as e:
                    sys.stderr.write(e.message)
                    workbook.clear()
                    sys.exit(1)
                if row_number - correction >= RESTRICTION_ROW_ON_SHEET - 1:
                    worksheet = workbook.add_worksheet()
                    correction = sheet * RESTRICTION_ROW_ON_SHEET
                    sheet += 1
                yield row_number
        if not self.silence:
            sys.stdout.write("\rConverted time: % .2fs\n" % (time.time() - begin_converted))
            sys.stdout.write("Writen xlsx file. It may take a long time.")
            sys.stdout.flush()
            begin_saving_time = time.time()
        workbook.close()
        if not self.silence:
            sys.stdout.write("\rTime saving: % .2fs                       \n" %
                             (time.time() - begin_saving_time))

    def __do_it_silence(self):

        performance = self.__make_xlsx()
        for i in performance:
            pass

    def __do_it_verbosity(self):

        begin_calculated_percent = time.time()
        try:
            line_count = sum(1 for line in open(self.csvfile, 'rb'))
        except IOError as e:
            sys.stderr.write(str(e) + '\n')
            sys.exit(1)
        sys.stdout.write("Time of calculating the line count: % .2fs\n" %
                         (time.time() - begin_calculated_percent))

        performance = self.__make_xlsx()

        percent = line_count / 100.0
        past = 0
        for i in performance:
            current = i // percent
            if past != current:
                sys.stdout.write('\rprocessed %s%%' % (current))
                sys.stdout.flush()
            past = current
