# -*- coding: utf8 -*-


class sharedStrings(object):

    """Accumulation unique values.
       This class need to fill a file shareStrings.xml,
       which conteining in xlsx-archive."""

    def __init__(self):
        self.__lst = dict()
        self.__count = 0
        self.__unique_count = 0

    @property
    def get_list(self):
        return self.__lst.keys()

    def add(self, val):
        """The main method of class."""
        self.__count += 1
        if val not in self.__lst:
            self.__lst[val] = self.__unique_count
            self.__unique_count += 1
        return self.__lst[val]

    def save(self, path):
        """When all data is calculated,
           then written it in shareStrings.xml."""

        with open(path + '/xl/sharedStrings.tmpl', 'r') as template:
            header = template.read().format(self.__count, self.__unique_count)
        with open(path + '/xl/sharedStrings.xml', 'w+') as obj:
            obj.write(header)
            tmp_dict = {value: key for key, value in self.__lst.items()}
            lst = [tmp_dict.get(i) for i in range(self.__unique_count)]
            for val in lst:
                val = val.replace('&', '&amp;')
                if val.endswith(' '):
                    obj.write('<si><t xml:space="preserve">%s</t></si>' % val)
                else:
                    obj.write('<si><t>%s</t></si>' % val)
            obj.write('</sst>')
