# -*- coding: utf8 -*-

import os
import sys
try:
    from exceptions import Exception  # Python 2
except ImportError:
    pass  # Python 3


# MS Excel restriction.
RESTRICTION_ROW_ON_SHEET = 1048576
RESTRICTION_COLUMNS_ON_SHEET = 16384

# LibreOffice Calc restriction
RESTRICTION_COLUMNS_ON_SHEET_LO = 1024

# Current path.
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3


class ExeededLimitCol(Exception):

    """Exceeded the limit on the number of columns."""

    def __init__(self, error):
        Exception.__init__(self, error)
        self.error = error
        self.message = 'Exceeded the limit on the number (%s) of columns.' % RESTRICTION_COLUMNS_ON_SHEET

    def __str__(self):
        return repr(self.message)
