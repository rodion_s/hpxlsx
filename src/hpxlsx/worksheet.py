# -*- coding: utf8 -*-

import sys
from itertools import product
from . import RESTRICTION_COLUMNS_ON_SHEET, PY2, ExeededLimitCol


class Worksheet(object):

    """Forming sheets of workbook."""

    def __init__(self, path, cn, shared, codepage):
        """
        @param:
              path - Path to temporary directory, where containing file for proccessing.
              cn - Consecutive number of this sheet.

        """
        self.codepage = codepage
        self.path = '{}/xl/worksheets/sheet{}.xml'.format(path, cn)
        self.number = 'tabSelected="1" ' if cn == 1 else ''
        self.template = '{}/xl/worksheets/worksheet.tmpl'.format(path)
        self.row_count = 0
        self.colist = self.gen_columns_number()
        self.unique_val = shared
        self.__cell = 'A1'
        self.__sheet_file = open(self.path, 'a')
        # We are remain places for header
        self.__sheet_file.write(' ' * 400)

    @staticmethod
    def gen_columns_number():
        """Generator all possible name columns for excel.
           Used one times at initialization."""

        symbols = ''.join([chr(i) for i in range(65, 91)])
        name_col = list()
        for i in range(1, 4):
            for res in product(symbols, repeat=i):
                name_col.append(''.join(res))
        return name_col[:RESTRICTION_COLUMNS_ON_SHEET]

    def append_row(self, row):
        """Append row to sheet from each transmited line."""

        index_list = None
        index_list = list()
        self.row_count += 1
        if len(row) < RESTRICTION_COLUMNS_ON_SHEET:
            self.colname = self.colist[len(row)]
        else:
            raise ExeededLimitCol
        row = self.encoding(row)
        for col in row:
            if col:
                index_list.append(self.unique_val.add(col))
            else:
                index_list.append(None)

        txt = '<row r="%s" spans="1:36">' % self.row_count
        for i, col in enumerate(index_list):
            if col is None:
                continue
            self.__cell = '%s%s' % (self.colist[i], self.row_count)
            txt += '<c r="%s" t="s"><v>%s</v></c>' % (self.__cell, col)
        txt += '</row>'
        self.__sheet_file.write(txt)

    def encoding(self, row):
        if self.codepage:
            try:
                if PY2:
                    row = [col.decode(self.codepage).encode('utf8') for col in row]
            except (LookupError, UnicodeError) as e:
                sys.stderr.write('Error: %s\n' % e)
                sys.exit(1)
        return row

    def save(self):
        """Finalize worksheet."""

        self.__sheet_file.write("""</sheetData>""")
        self.__sheet_file.write("""<pageMargins left="0.7" right="0.7" """)
        self.__sheet_file.write("""top="0.75" bottom="0.75" header="0.3" footer="0.3"/>""")
        self.__sheet_file.write("""</worksheet>""")
        self.__sheet_file.close()

        with open(self.template, 'r') as tmpl:
            if self.__cell == 'A1':
                header = tmpl.read().format(cell='', tab=self.number)
            else:
                header = tmpl.read().format(cell=':' + self.__cell, tab=self.number)

        with open(self.path, 'r+') as tmp:
            tmp.seek(0)
            tmp.write(header)
