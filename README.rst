======
hpxlsx
======

High-performance converter CSV to XSLX.

Designed to work with millions of rows. 

If the rows is more than acceptable on a single sheet, adds a new sheet and fills it.

It compatible with Python version 2 and 3, also PyPy.

Installation:
=============
::

    $ sudo pip install git+https://bitbucket.org/rodion_s/hpxlsx


Usage:
======

::

   $ hpxlsx -i some.csv

You may specify output file:

::

   $ hpxlsx -i some.csv -o myfile.xlsx


XLSX file demand only UTF8 codepage. If your CSV file in another one, then use argument -c, like:

::

  $ hpxlsx -i some.csv -c cp1251


And specify quiet mode:

::

   $ hpxlsx -i some.csv -s


License:
========

hpxlsx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

hpxlsx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hpxlsx.  If not, see http://www.gnu.org/licenses/.
